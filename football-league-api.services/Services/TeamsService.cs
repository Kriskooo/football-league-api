﻿using football_league_api.Common.Database;
using football_league_api.Common.Database.Models;
using football_league_api.Common.Exceptions;
using football_league_api.Interfaces;
using football_league_api.Request_ResponseModels;
using football_league_api.RequestModels;
using Microsoft.EntityFrameworkCore;

namespace football_league_api
{
    public class TeamsService : ITeamsService
    {
        private readonly ApplicationDbContext _dbContext;

        public TeamsService(ApplicationDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<TeamResponse> GetTeamAsync(int id)
        {
            return await _dbContext.Teams
                .Where(t => t.Id == id)
                .Select(t => new TeamResponse
                {
                    Id = t.Id,
                    Name = t.Name,
                    Points = t.Points
                })
                .FirstOrDefaultAsync() ?? throw new NotFoundException();
        }

        public async Task<IEnumerable<TeamResponse>> QueryAsync()
        {
            return await _dbContext.Teams
                .Select(t => new TeamResponse
                {
                    Id = t.Id,
                    Name = t.Name,
                    Points = t.Points
                })
                .ToListAsync();
        }

        public async Task InsertAsync(InsertTeamRequest request)
        {
            Team team = new()
            {
                Name = request.Name
            };

            _dbContext.Teams.Add(team);
            await _dbContext.SaveChangesAsync();
        }

        public async Task PatchAsync(int id, PatchTeamRequest request)
        {
            Team team = await _dbContext.Teams.FindAsync(id) ?? throw new NotFoundException();

            team.Name = request.Name;
            team.Points = request.Points;
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            Team team = await _dbContext.Teams.FindAsync(id) ?? throw new NotFoundException();
            _dbContext.Teams.Remove(team);
            await _dbContext.SaveChangesAsync();
        }
    }
}
