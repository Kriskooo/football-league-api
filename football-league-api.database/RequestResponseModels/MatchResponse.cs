﻿namespace football_league_api.Request_ResponseModels
{
    public class MatchResponse
    {
        public int Id { get; set; }
        public int HostTeamId { get; set; }
        public int GuestTeamId { get; set; }
        public int HostTeamScore { get; set; }
        public int GuestTeamScore { get; set; }
        public bool IsPlayed { get; set; }
    }
}
