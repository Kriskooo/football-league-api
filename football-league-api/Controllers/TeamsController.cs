﻿using football_league_api.Common.Database.Models;
using football_league_api.Interfaces;
using football_league_api.RequestModels;
using Microsoft.AspNetCore.Mvc;

namespace football_league_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;

        public TeamsController(ITeamsService teamsService) =>
            _teamsService = teamsService;

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Team>>> QueryAsync() =>
            Ok(await _teamsService.QueryAsync());

        [HttpGet("{id}")]
        public async Task<ActionResult<Team>> GetAsync(int id) =>
           Ok(await _teamsService.GetTeamAsync(id));

        [HttpPost]
        public async Task InsertAsync(InsertTeamRequest request) =>
            await _teamsService.InsertAsync(request);

        [HttpPatch("{id}")]
        public async Task PatchAsync(int id, PatchTeamRequest request) =>
            await _teamsService.PatchAsync(id, request);

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id) =>
            await _teamsService.DeleteAsync(id);
    }
}
