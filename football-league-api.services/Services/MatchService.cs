﻿using football_league_api.Common.Database;
using football_league_api.Common.Database.Models;
using football_league_api.Common.Exceptions;
using football_league_api.Interfaces;
using football_league_api.Request_ResponseModels;
using Microsoft.EntityFrameworkCore;

namespace football_league_api.Services
{
    public class MatchService : IMatchService
    {
        private readonly ApplicationDbContext _dbContext;

        public MatchService(ApplicationDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<MatchResponse> GetMatchAsync(int id)
        {
            return await _dbContext.Matches
                .Where(m => m.Id == id)
                .Select(m => new MatchResponse
                {
                    Id = m.Id,
                    HostTeamId = m.HostTeamId,
                    GuestTeamId = m.GuestTeamId,
                    HostTeamScore = m.HostTeamScore,
                    GuestTeamScore = m.GuestTeamScore,
                })
                .FirstOrDefaultAsync() ?? throw new NotFoundException();
        }

        public async Task<IEnumerable<MatchResponse>> GetPlayedMatchesAsync()
        {
            return await _dbContext.Matches
                .Select(m => new MatchResponse
                {
                    Id = m.Id,
                    HostTeamId = m.HostTeamId,
                    GuestTeamId = m.GuestTeamId,
                    HostTeamScore = m.HostTeamScore,
                    GuestTeamScore = m.GuestTeamScore,
                    IsPlayed = true

                })
                .ToListAsync();
        }

        public async Task InsertAsync(InsertMatchRequest request)
        {
            if (request.IsPlayed)
            {
                Match match = new()
                {
                    HostTeamId = request.HostTeamId,
                    GuestTeamId = request.GuestTeamId,
                    HostTeamScore = request.HostTeamScore,
                    GuestTeamScore = request.GuestTeamScore,
                    IsPlayed = true
                };
                _dbContext.Matches.Add(match);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new InvalidOperationException("Cannot insert a match that is not played.");
            }

        }

        public async Task UpdateAsync(int id, InsertMatchRequest request)
        {
            Match match = await _dbContext.Matches.FindAsync(id) ?? throw new NotFoundException();

            if (match.IsPlayed)
            {

                match.HostTeamId = request.HostTeamId;
                match.GuestTeamId = request.GuestTeamId;
                match.HostTeamScore = request.HostTeamScore;
                match.GuestTeamScore = request.GuestTeamScore;

                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new InvalidOperationException("Cannot update a match that is not played.");
            }

        }

        public async Task DeleteAsync(int id)
        {
            Match match = await _dbContext.Matches.FindAsync(id) ?? throw new NotFoundException();
            _dbContext.Matches.Remove(match);
            await _dbContext.SaveChangesAsync();
        }
    }
}

