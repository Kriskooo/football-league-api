Football League API
This is an API for a football league system. It provides endpoints for managing teams and matches, and for viewing team rankings.

Features:
Teams Management: The API provides endpoints for creating, reading, updating, and deleting teams.
Matches Management: The API provides endpoints for creating, reading, updating, and deleting matches. When a match is created or updated, the teams' points are updated based on the match result.
Rankings: The API provides an endpoint for viewing the current rankings of the teams. The teams are sorted by their points in descending order, and each team's details include the number of matches they've played.
Scoring System: The system follows these scoring rules: win – 3 points, draw – 1 point, loss – 0 points.

Technologies
ASP.NET Core 6: The API is built with ASP.NET Core 6.
Entity Framework Core: The API uses Entity Framework Core for data persistence. It uses a SQL Server database.
Swagger: The API includes a Swagger UI for exploring and testing the endpoints.

Setup: To run the API, you'll need to have .NET 6.0 SDK installed. You can then run the API using the dotnet run command.

You'll also need to have a SQL Server database available, and you'll need to update the connection string in the appsettings.json file.

Usage
Once the API is running, you can access the Swagger UI at https://localhost:5001/swagger. From there, you can explore and test the endpoints.