﻿using football_league_api.Request_ResponseModels;

namespace football_league_api.Interfaces
{
    public interface IMatchService
    {
        Task<MatchResponse> GetMatchAsync(int id);
        Task<IEnumerable<MatchResponse>> GetPlayedMatchesAsync();
        Task InsertAsync(InsertMatchRequest request);
        Task UpdateAsync(int id, InsertMatchRequest request);
        Task DeleteAsync(int id);
    }
}
