﻿using football_league_api.database.Common.Database.Models;

namespace football_league_api.Common.Database.Models
{
    public class Match : BaseEntity
    {
        public int Id { get; set; }
        public int HostTeamScore { get; set; }
        public int GuestTeamScore { get; set; }
        public DateTimeOffset MatchDate { get; set; }
        public int HostTeamId { get; set; }
        public Team HostTeam { get; set; }
        public int GuestTeamId { get; set; }
        public Team GuestTeam { get; set; }
        public bool IsPlayed { get; set; }

        public bool IsMatchPlayed()
        {
            return MatchDate <= DateTimeOffset.UtcNow;
        }

        public override string ToString()
        {
            return $"{HostTeam.Name} {HostTeamScore} - {GuestTeamScore} {GuestTeam.Name}";
        }
    }
}
