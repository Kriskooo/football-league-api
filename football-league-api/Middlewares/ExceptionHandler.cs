using football_league_api.Common.Exceptions;
using football_league_api.Middlewares.Models;

namespace football_league_api.Middlewares;

public class ExceptionHandler
{
    private readonly RequestDelegate _next;
    private readonly ILogger<ExceptionHandler> _logger;

    public ExceptionHandler(RequestDelegate next, ILogger<ExceptionHandler> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception.Message, exception);
            await HandleExceptionAsync(httpContext, exception);
        }
    }

    private static Task HandleExceptionAsync(HttpContext context, Exception exception)
    {
        context.Response.ContentType = "application/json";

        var (statusCode, message) = exception switch
        {
            ArgumentException _ => (StatusCodes.Status400BadRequest, "The request you provided consist invalid data."),
            NotFoundException _ => (StatusCodes.Status404NotFound, "The requested resource was not found."),
            _ => (StatusCodes.Status500InternalServerError, "You request encountered server error.")
        };

        context.Response.StatusCode = statusCode;

        ExeceptionDetail response = new()
        {
            StatusCode = statusCode,
            Message = message
        };

        return context.Response.WriteAsync(response.ToString());
    }
}
