﻿using football_league_api.Common.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace football_league_api.Common.Database.Configurations
{
    public class MatchesConfiguration : IEntityTypeConfiguration<Match>
    {
        public void Configure(EntityTypeBuilder<Match> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.MatchDate)
                .IsRequired();

            builder.HasOne(m => m.GuestTeam)
                .WithMany(t => t.GuestMatches)
                .HasForeignKey(m => m.GuestTeamId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(m => m.HostTeam)
                .WithMany(t => t.HostedMatches)
                .HasForeignKey(m => m.HostTeamId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}