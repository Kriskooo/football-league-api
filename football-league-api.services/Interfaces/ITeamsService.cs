﻿using football_league_api.Request_ResponseModels;
using football_league_api.RequestModels;

namespace football_league_api.Interfaces
{
    public interface ITeamsService
    {
        Task DeleteAsync(int id);
        Task<TeamResponse> GetTeamAsync(int id);
        Task InsertAsync(InsertTeamRequest request);
        Task PatchAsync(int id, PatchTeamRequest request);
        Task<IEnumerable<TeamResponse>> QueryAsync();
    }
}
