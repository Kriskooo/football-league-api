﻿using System.ComponentModel.DataAnnotations;

namespace football_league_api.RequestModels
{
    public class InsertTeamRequest
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}
