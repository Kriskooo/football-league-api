﻿namespace football_league_api.Request_ResponseModels
{
    public class TeamResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long Points { get; set; }
    }
}
