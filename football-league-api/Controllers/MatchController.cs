﻿using football_league_api.Common.Database.Models;
using football_league_api.Interfaces;
using football_league_api.Request_ResponseModels;
using Microsoft.AspNetCore.Mvc;

namespace football_league_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MatchController : ControllerBase
    {
        private readonly IMatchService _matchService;

        public MatchController(IMatchService matchService) =>
            _matchService = matchService;

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Match>>> QueryAsync() =>
            Ok(await _matchService.GetPlayedMatchesAsync());

        [HttpGet("{id}")]
        public async Task<ActionResult<Match>> GetMatchAsync(int id) =>
           Ok(await _matchService.GetMatchAsync(id));

        [HttpPost]
        public async Task InsertAsync(InsertMatchRequest request) =>
            await _matchService.InsertAsync(request);

        [HttpPatch("{id}")]
        public async Task UpdateAsync(int id, InsertMatchRequest request) =>
            await _matchService.UpdateAsync(id, request);

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id) =>
            await _matchService.DeleteAsync(id);
    }
}
