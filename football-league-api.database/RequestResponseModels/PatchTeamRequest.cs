﻿using System.ComponentModel.DataAnnotations;

namespace football_league_api.RequestModels
{
    public class PatchTeamRequest
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public long Points { get; set; }
    }
}
