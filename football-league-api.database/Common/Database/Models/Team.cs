﻿using football_league_api.database.Common.Database.Models;

namespace football_league_api.Common.Database.Models
{
    public class Team : BaseEntity
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public long Points { get; set; }
        public ICollection<Match> HostedMatches { get; set; }
        public ICollection<Match> GuestMatches { get; set; }
    }
}
